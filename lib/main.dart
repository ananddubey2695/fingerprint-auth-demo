import 'package:flutter/material.dart';
import 'finger_print_bloc.dart';

void main() => runApp(MaterialApp(
      home: LoginScreen(),
    ));

class LoginScreen extends StatefulWidget {
  @override
  LoginScreenState createState() {
    return new LoginScreenState();
  }
}

class LoginScreenState extends State<LoginScreen> {
  String authOutputText = "";

  @override
  Widget build(BuildContext context) {
    return Material(
      type: MaterialType.transparency,
      child: Container(
        color: Color(0xff1976d2),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              RaisedButton(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
                color: Color(0xff004ba0),
                child: Text(
                  "Login using Fingerprint",
                  style: TextStyle(color: Colors.white),
                ),
                onPressed: () {
                  bloc.authenticateFingerPrint().asStream().listen((data) {
                    print(data);
                    setState(() {
                      if (data)
                        authOutputText = "Successfully Logged in";
                      else
                        authOutputText =
                            "Authentication Failed.. \n Please try again";
                    });
                  });
                },
              ),
              Text(
                authOutputText,
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 18,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
