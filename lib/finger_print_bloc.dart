import 'package:local_auth/local_auth.dart';

class FingerPrintBloc {
  var localAuth = LocalAuthentication();

  Future authenticateFingerPrint() async {
    return await localAuth.authenticateWithBiometrics(
        localizedReason: 'Please authenticate to Login');
  }
}

final bloc = FingerPrintBloc();
